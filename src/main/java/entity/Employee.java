package entity;


import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "employees")
public class Employee extends BaseEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @Expose
    private Role role;

    @ManyToMany
    private Set<Project> projects;

    @OneToMany
    private Set<Dependent> dependent;

    @Expose
    private String data;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
      this.data = data;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Set<Dependent> getDependent() {
        return dependent;
    }

    public void setDependent(Set<Dependent> dependent) {
        this.dependent = dependent;
    }
}