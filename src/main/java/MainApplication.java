import com.google.gson.Gson;
import dao.*;
import entity.Department;
import entity.Dependent;
import entity.Employee;
import entity.Project;
import model.Secretary;
import util.ActionsUtil;
import util.ConstantsUtil;
import util.HibernateOGMUtil;

import java.util.Calendar;
import java.util.Date;

public class MainApplication {
    public static void main(String[] args) {
        HibernateOGMUtil.getInstance();
        //RoleSeeder.execute(HibernateOGMUtil.getEntityManager());

        /* Common */
        EmployeeDAO employeeDAO = new EmployeeDAO();
        DepartmentDAO departmentDAO = new DepartmentDAO();
        RoleDAO roleDAO = new RoleDAO();


        /* Department */
        boolean execDepartment = false;
        if(execDepartment) {
            Department department = new Department();
            department.setId("80501724-fccb-4783-81b1-05dacb599db5");
            department.setName("DEP LUIS");
            department.setNumber(123);

            ActionsUtil.department(department, ConstantsUtil.UPDATE_DATA);

        }

        /* Employee */
        /*

            1. (Secretário)
            2. (Pesquisador)
            3. (Auxiliar de Limpeza)

         */

        boolean execEmployee = true;
        if(execEmployee){
            Secretary secretary = new Secretary(
                    "Luis Fernando da Masceno",
                    "Rua 2",
                    "M",
                    "14/07/1997",
                    12000.0,
                    "SUPERIOR 2"
            );

            Employee employee = new Employee();
            employee.setId("7a440ba7-b22a-4c21-85f2-c71b8f2d60a9");
            employee.setRole(roleDAO.findByName("Secretário"));
            employee.setData(new Gson().toJson(secretary, Secretary.class));

            ActionsUtil.employee(employee, ConstantsUtil.UPDATE_DATA);
        }


        /* Project */
        boolean execProject = false;
        if (execProject) {

            Project project = new Project();
            project.setId("2b38b276-7b10-41ca-9439-53a9dd17b23f");
            project.setDate(new Date());
            project.setHour("10:32");
            project.setDepartment(departmentDAO.findById("80501724-fccb-4783-81b1-05dacb599db5"));
            project.setEmployees(employeeDAO.findAll());

            ActionsUtil.project(project, ConstantsUtil.DELETE_DATA);
        }

        /* Dependent */
        boolean execDependent = false;
        if(execDependent) {

            Dependent dependent = new Dependent();
            dependent.setId("3294cc16-9260-4022-b044-b007ce3c0255");
            dependent.setBirthday(new Date(1997, Calendar.JULY, 14));
            dependent.setEmployee(employeeDAO.findById("7a440ba7-b22a-4c21-85f2-c71b8f2d60a9"));
            dependent.setKinship("Irmão");
            dependent.setName("Luis 2");
            dependent.setSex("M");

            ActionsUtil.dependent(dependent, ConstantsUtil.FIND_ALL_DATA);
        }
    }
}