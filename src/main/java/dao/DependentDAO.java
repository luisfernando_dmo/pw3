package dao;

import entity.Dependent;
import entity.Project;
import interfaces.DependentInterface;
import util.HibernateOGMUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class DependentDAO implements DependentInterface {

    private EntityTransaction transaction;
    private EntityManager entityManager;

    public DependentDAO(){
        this.entityManager = HibernateOGMUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(Dependent entity) {
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(Dependent entity) {
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public Dependent findById(String id) {
        return entityManager.find(Dependent.class, id);
    }

    @Override
    public boolean delete(Dependent entity) {
        try{
            transaction.begin();
            entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<Dependent> findAll() {
        return entityManager.createNativeQuery("db.dependents.find({})", Dependent.class).getResultList();
    }
}