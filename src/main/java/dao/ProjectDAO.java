package dao;

import entity.Employee;
import entity.Project;
import interfaces.ProjectInterface;
import util.HibernateOGMUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class ProjectDAO implements ProjectInterface {

    private EntityTransaction transaction;
    private EntityManager entityManager;

    public ProjectDAO(){
        this.entityManager = HibernateOGMUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(Project entity) {
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(Project entity) {
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public Project findById(String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public boolean delete(Project entity) {
        try{
            transaction.begin();
            entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<Project> findAll() {
        return entityManager.createNativeQuery("db.projects.find({})", Project.class).getResultList();
    }
}
