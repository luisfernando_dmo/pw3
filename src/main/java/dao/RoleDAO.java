package dao;

import entity.Role;
import interfaces.RoleInterface;
import util.HibernateOGMUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.io.IOException;
import java.util.List;

public class RoleDAO implements RoleInterface {

    private EntityTransaction transaction;
    private EntityManager entityManager;

    public RoleDAO(){
        this.entityManager = HibernateOGMUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(Role entity) {
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(Role entity) {
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public Role findById(String id) {
        return entityManager.find(Role.class, id);
    }

    @Override
    public Role findByName(String name) {
        return (Role) entityManager.createNativeQuery("db.roles.find({'name': '"  + name + "'})", Role.class).getResultList().get(0);
    }

    @Override
    public boolean delete(Role entity) {
        try{
            transaction.begin();
            entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<Role> findAll() {
        return entityManager.createNativeQuery("db.roles.find({})", Role.class).getResultList();

    }
}