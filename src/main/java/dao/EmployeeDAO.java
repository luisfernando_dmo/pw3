package dao;

import entity.Department;
import entity.Employee;
import interfaces.EmployeeInterface;
import util.HibernateOGMUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class EmployeeDAO implements EmployeeInterface {

    private EntityTransaction transaction;
    private EntityManager entityManager;

    public EmployeeDAO(){
        this.entityManager = HibernateOGMUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(Employee entity) {
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(Employee entity) {
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public Employee findById(String id) {
        return entityManager.find(Employee.class, id);
    }

    @Override
    public boolean delete(Employee entity) {
        try{
            transaction.begin();
            entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<Employee> findAll() {
        return entityManager.createNativeQuery("db.employees.find({})", Employee.class).getResultList();
    }
}