package dao;

import entity.Department;
import interfaces.DepartmentInterface;
import util.HibernateOGMUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class DepartmentDAO implements DepartmentInterface {
    private EntityTransaction transaction;
    private EntityManager entityManager;

    public DepartmentDAO(){
        this.entityManager = HibernateOGMUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(Department entity) {
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(Department entity) {
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public Department findById(String id) {
        return entityManager.find(Department.class, id);
    }

    @Override
    public boolean delete(Department entity) {
        try{
            transaction.begin();
            entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<Department> findAll() {
        return entityManager.createNativeQuery("db.departments.find({})", Department.class).getResultList();
    }
}