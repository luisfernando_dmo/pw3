package seed;

import entity.Role;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

public class RoleSeeder {
    private static List<Role> roles;

    public static void execute(EntityManager entityManager){
        RoleSeeder.seed();

        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            entityManager.createNativeQuery("db.roles.remove({})").executeUpdate();
            for(Role role: RoleSeeder.roles){
                entityManager.persist(role);
            }
            transaction.commit();
        }catch (PersistenceException e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
    }

    private static void seed(){
        RoleSeeder.roles = new ArrayList<>();

        RoleSeeder.roles.add(new Role("Pesquisador"));
        RoleSeeder.roles.add(new Role("Secretário"));
        RoleSeeder.roles.add(new Role("Auxiliar de Limpeza"));
    }
}