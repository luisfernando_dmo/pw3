package model;

import java.io.Serializable;

public class Secretary implements Serializable {
    private String name;
    private String address;
    private String sex;
    private String birthday;
    private Double salary;
    private String schooling;

    public Secretary(){}

    public Secretary(String name, String address, String sex, String birthday, Double salary, String schooling) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.birthday = birthday;
        this.salary = salary;
        this.schooling = schooling;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getSchooling() {
        return schooling;
    }

    public void setSchooling(String schooling) {
        this.schooling = schooling;
    }
}
