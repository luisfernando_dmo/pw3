package util;

public class ConstantsUtil {
    public static final int PERSIST_DATA = 1;
    public static final int UPDATE_DATA = 2;
    public static final int DELETE_DATA = 3;
    public static final int FIND_ALL_DATA = 4;
}