package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil<T> {
    public T getJSONObjectData (String data, Class<T> type){
        return new Gson().fromJson(data, type);
    }

    public void printJSON(Object object, Class<T> type){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        String json = gson.toJson(object, type);
        System.out.println(json);
    }
}