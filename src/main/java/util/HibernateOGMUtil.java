package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateOGMUtil {
    private static HibernateOGMUtil instance = null;

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    private HibernateOGMUtil(){
        entityManagerFactory = Persistence.createEntityManagerFactory("ogm-pw3");
    }

    public static HibernateOGMUtil getInstance(){
        if(instance == null){
            instance  = new HibernateOGMUtil();
        }
        return instance;
    }

    public static EntityManagerFactory getSessionFactory() {
        return entityManagerFactory;
    }

    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
