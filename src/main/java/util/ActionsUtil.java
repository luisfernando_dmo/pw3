package util;

import dao.DepartmentDAO;
import dao.DependentDAO;
import dao.EmployeeDAO;
import dao.ProjectDAO;
import entity.Department;
import entity.Dependent;
import entity.Employee;
import entity.Project;

public class ActionsUtil {
    public static void dependent(Dependent entity, int action){

        DependentDAO dao = new DependentDAO();

        switch (action){
            case ConstantsUtil.PERSIST_DATA:
                dao.persist(entity);
                break;

            case ConstantsUtil.UPDATE_DATA:
                dao.update(entity);
                break;

            case ConstantsUtil.DELETE_DATA:
                dao.delete(entity);
                break;

            case ConstantsUtil.FIND_ALL_DATA:
                for(Dependent d: dao.findAll()){
                    new GsonUtil<Dependent>().printJSON(d, Dependent.class);
                }
                break;
        }
    }

    public static void department(Department entity, int action){

        DepartmentDAO dao = new DepartmentDAO();

        switch (action){
            case ConstantsUtil.PERSIST_DATA:
                dao.persist(entity);
                break;

            case ConstantsUtil.UPDATE_DATA:
                dao.update(entity);
                break;

            case ConstantsUtil.DELETE_DATA:
                dao.delete(entity);
                break;

            case ConstantsUtil.FIND_ALL_DATA:
                for(Department d: dao.findAll()){
                    new GsonUtil<Department>().printJSON(d, Department.class);
                }
                break;
        }
    }


    public static void employee(Employee entity, int action){

        EmployeeDAO dao = new EmployeeDAO();

        switch (action){
            case ConstantsUtil.PERSIST_DATA:
                dao.persist(entity);
                break;

            case ConstantsUtil.UPDATE_DATA:
                dao.update(entity);
                break;

            case ConstantsUtil.DELETE_DATA:
                dao.delete(entity);
                break;

            case ConstantsUtil.FIND_ALL_DATA:
                for(Employee d: dao.findAll()){
                    new GsonUtil<Employee>().printJSON(d, Employee.class);
                }
                break;
        }
    }


    public static void project(Project entity, int action){

        ProjectDAO dao = new ProjectDAO();

        switch (action){
            case ConstantsUtil.PERSIST_DATA:
                dao.persist(entity);
                break;

            case ConstantsUtil.UPDATE_DATA:
                dao.update(entity);
                break;

            case ConstantsUtil.DELETE_DATA:
                dao.delete(entity);
                break;

            case ConstantsUtil.FIND_ALL_DATA:
                for(Project d: dao.findAll()){
                    new GsonUtil<Project>().printJSON(d, Project.class);
                }
                break;
        }
    }
}