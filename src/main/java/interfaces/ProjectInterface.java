package interfaces;

import entity.Project;

import java.util.List;

public interface ProjectInterface {
    public boolean persist(Project entity);

    public boolean update(Project entity);

    public Project findById(String id);

    public boolean delete(Project entity);

    public List<Project> findAll();
}
