package interfaces;

import entity.Dependent;

import java.util.List;

public interface DependentInterface {
    public boolean persist(Dependent entity);

    public boolean update(Dependent entity);

    public Dependent findById(String id);

    public boolean delete(Dependent entity);

    public List<Dependent> findAll();
}