package interfaces;

import entity.Department;

import java.util.List;

public interface DepartmentInterface {
    public boolean persist(Department entity);

    public boolean update(Department entity);

    public Department findById(String id);

    public boolean delete(Department entity);

    public List<Department> findAll();
}
