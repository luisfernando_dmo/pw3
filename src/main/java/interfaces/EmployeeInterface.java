package interfaces;

import entity.Employee;

import java.util.List;

public interface EmployeeInterface {
    public boolean persist(Employee entity);

    public boolean update(Employee entity);

    public Employee findById(String id);

    public boolean delete(Employee entity);

    public List<Employee> findAll();
}
