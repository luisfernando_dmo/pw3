package interfaces;

import entity.Role;

import java.util.List;

public interface RoleInterface {
    public boolean persist(Role entity);

    public boolean update(Role entity);

    public Role findById(String id);

    public Role findByName(String name);

    public boolean delete(Role entity);

    public List<Role> findAll();
}